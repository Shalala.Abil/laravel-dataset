<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlayerController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/upload', [PlayerController::class, 'index']);

Route::post('/uploads', [PlayerController::class, 'uploadContent'])->name('import.content');

Route::get('/export', [PlayerController::class, 'export']);









