<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

use Illuminate\Support\Facades\DB;
use App\Models\Player;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Response;

use App\Mail\MailNotify;



 class PlayerController extends Controller
{
    public function index(){

            $data['count'] = DB::table('players')->count();

            $query = DB::table('players')->orderBy('id', 'asc');
            if($keyword=request('search')){
                $data['paginator']=
                $query->orwhere('birthDate','LIKE', '%'.$keyword.'%')
                ->orWhere('category', 'LIKE', '%'.$keyword.'%')
                ->orWhere('firstname', 'LIKE', '%'.$keyword.'%')
                ->orWhere('lastname', 'LIKE', '%'.$keyword.'%')
                ->orWhere('email', 'LIKE', '%'.$keyword.'%')
                ->orWhere('gender', 'LIKE', $keyword.'%')->paginate(10);
            }
            else{
                $data['paginator']=$query->paginate(10);
            }

            $data['players']  = $data['paginator'] 
            ->getCollection()
            ->map(function($item) {
                return $item ;
            })->toArray();

            return view('welcomeEmail', $data);
        
    }


    public function uploadContent(Request $request){     

        $file = $request->file('uploaded_file');
        if ($file) {
            $fileName = $file->getClientOriginalName();
            $extension = pathinfo($fileName, PATHINFO_EXTENSION);
            $tempPath = realpath($file) ;
            $fileSize = $file->getSize(); 
            $this->checkUploadedFileProperties($extension, $fileSize);

            $location= 'uploadss';

            if (!file_exists($location)) mkdir($location, 0777, true);
            
            $file->move($location, $fileName);
            $filepath = public_path($location . "/" . $fileName);
            $file = fopen($filepath, "r");
            $importData_arr = array();
            $i = 0;
            
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                $num = count($filedata);
                if ($i == 0) {
                    $i++;
                    continue;
                }
                for ($c = 0; $c < $num; $c++) {
                    $importData_arr[$i] = explode(",", $filedata[$c]) ;
                }
                $i++;
            }
        
            fclose($file); 
            $j = 0;
            $query= "insert into players (category,firstname,lastname,email,gender,birthDate) values" ;
            foreach ($importData_arr as $importData) {
                $j++;
                $importData[1]=str_replace("'","",$importData[2]);
                $importData[2]=str_replace("'","",$importData[2]);
                $query .=  "  ('". $importData[0]."','". $importData[1]."','". $importData[2]."','". $importData[3]."','". $importData[4]."','". $importData[5]."'),";    
            }

            try {
                DB::statement(DB::raw(substr($query, 0, -1)));
            } 
            catch (\Exception $e) {
                DB::rollBack();
            }

        
            print response()->json([
            'message' => "$j records successfully uploaded"
            ]);
            // $this->sendMail($j);

            return redirect()->to('/upload')->with('success', 'Successfully added a user account');


        } 
        else {
            throw new \Exception('No file was uploaded1', Response::HTTP_BAD_REQUEST);
        }
    }
    public function checkUploadedFileProperties($extension, $fileSize){
        $valid_extension = array("csv", "xlsx"); 
        $maxFileSize = 2097152; 

        if (!in_array(strtolower($extension), $valid_extension))    throw new \Exception('Invalid file extension', Response::HTTP_UNSUPPORTED_MEDIA_TYPE); //415 error
    
    }



    public function export (){
            
        header('Content-Type: text/csv; charset=utf-8');  
        header('Content-Disposition: attachment; filename=data.csv');  
        
        $output = fopen("php://output", "w");  

        fputcsv($output, array('category','firstname','lastname','email','gender','birthDate')); 

        $query=DB::table('players')->select('category','firstname','lastname','email','gender','birthDate')->get()->toArray();       

        $query= json_decode(json_encode($query), true);

        foreach ($query as $line) {
            fputcsv($output, array_values($line));
        }

        
        fclose($output);


    }

    public function sendMail($j)
    {
        $data = [
            "subject"=>"Success",
            "body"=>"$j record succesfully added."
            ];

        try
        {
            Mail::to('abilova.shalala@gmail.com')->send(new MailNotify($data));
            return response()->json(['Great! Successfully send in your mail']);
        }
        catch(Exception $e)
        {
            return response()->json(['Sorry! Please try again latter']);
        }
        } 


}