 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" />


<div style="margin-left:20px;" class="container">



  <form action="uploads" method="POST"  enctype="multipart/form-data">
  @csrf

    <div class="form-group">
    <div class="col-md-12 col-md-offset-4">
        Select CSV dataset  to upload:
        <input type="file" name="uploaded_file" id="uploaded_file">
        <input type="submit" value="Upload" name="submit">

    </div>

  </form>

  <br>

  <div class="row">


    <div class="col-sm-12">

      <form action="" method="get"  enctype="multipart/form-data"
      >
      <!-- @csrf -->

      <input type="input" name="search" id="search">
      <input type="submit" value="search" name="submit">
      <button id="back">Back</button>
      </form>

      <div class="mail-option">
        <table border=1 class="table table-inbox table-hover table-bordered text-center" id="datatable">
          <thead>
            <tr>
              <th>id</th>
              <th>category</th>
              <th>firstname</th>
              <th>lastname</th>
              <th>email</th>
              <th>gender</th>
              <th>birthDate</th>
            </tr>
          </thead>
        <tbody>
          <?php 
          foreach( json_decode(json_encode($players), true) as  $key => $val) { ?>          
            <tr>
              <td><?=$val['id']?></td>
              <td><?=$val['category']?></td>
              <td><?=$val['firstname']?></td>
              <td><?=$val['lastname']?></td>
              <td><?=$val['email']?></td>
              <td><?=$val['gender']?></td>
              <td><?=$val['birthDate']?></td>
            </tr>
          <?php } ?>

        </tbody>
        </table>
        </div>
      </div>

      <div class="row">{{ $paginator->links() }} </div>

  </div>


  <form action="export"   method="get"  enctype="multipart/form-data">
  @csrf

    <div class="form-group">
    <div class="col-md-12 col-md-offset-4">
        <input type="submit" name="export" class="btn btn-success" value="export to CSV"/>
    </div>


  </form>
  </div>
<script>
$(document).on('click', '#back', function () {
	  window.location.reload();
 }  )    
 </script>